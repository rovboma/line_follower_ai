# -*- coding: utf-8 -*-
"""
Created on Sun Mar 19 22:01:33 2017

@author: Maxim Rovbo

Thanks to https://www.pygame.org/docs/ref/draw.html
"""

import math

import simulation as sim
import pygame

import visualization
import controllers

import shapely.geometry as geom

def wait():
    while True:
        for event in pygame.event.get():
            if event.type == pygame.QUIT or event.type == pygame.KEYDOWN:
                pygame.quit()
                return
                
def chooseOnOffController(simulation, freq) :
    """ Configure and attach a simple On/Off controller to the simulation.
    
    Uses manually configured ON-OFF rules.
    """
    simulation.robot.attachController(controllers.OnOffController(freq))
         
def chooseANNController(simulation, freq) :
    """ Attach an ANN rule imitation controller to the simulation.
    
    This controller learns to imitate the given situation->speed rules (
    solves regression).
    """
    ideal_ins = [
            [0., 0., 0.],
            [0., 0., 1.],
            [0., 1., 0.],
            [0., 1., 1.],
            [1., 0., 0.],
            [1., 0., 1.],
            [1., 1., 0.],
            [1., 1., 1.],
        ]
        
    TURN_SPEED = 0.05
    FWD_SPEED = 0.2
    
    ideal_outs = [
        [-TURN_SPEED + FWD_SPEED, TURN_SPEED + FWD_SPEED],
        [TURN_SPEED + FWD_SPEED, -TURN_SPEED + FWD_SPEED], # right
        [FWD_SPEED, FWD_SPEED],
        [FWD_SPEED, FWD_SPEED],
        [-TURN_SPEED + FWD_SPEED, TURN_SPEED + FWD_SPEED], # left
        [FWD_SPEED, FWD_SPEED],
        [FWD_SPEED, FWD_SPEED],
        [FWD_SPEED, FWD_SPEED],
    ]
    
    ann_controller = controllers.ANNController(freq, ideal_ins, ideal_outs, True)
    ann_controller.train()
    
    simulation.robot.attachController(ann_controller)
         
def chooseGASimpleRuleController(simulation, freq) :
    """ Attach a GA chromosome rule imitation controller to the simulation.
    
    The algorithm is based on a genetic representation of a strategy. The
    required strategy is given as a chromosome and the genetic algorithm needs
    to copy it.
    """
    ideal_ins = [
        [0., 0., 0.],
        [0., 0., 1.],
        [0., 1., 0.],
        [0., 1., 1.],
        [1., 0., 0.],
        [1., 0., 1.],
        [1., 1., 0.],
        [1., 1., 1.],
    ]
    
    ideal_outs = [0,0,0,1, 0,1,0,0, 0,1,0,1, 0,1,0,1, 
                  0,0,0,1, 0,1,0,1, 0,1,0,1, 0,1,0,1, ]
    
    optimizer = controllers.GAImitationOptimizer(freq, ideal_ins, ideal_outs)
    best_chromosome = optimizer.optimize()
    
    ga_controller = controllers.GAController(freq, best_chromosome)
            
    simulation.robot.attachController(ga_controller)
         
def chooseGASpeedRuleController(simulation, freq) :
    """ Attach a GA speed rule imitation controller to the simulation.
    
    The algorithm is based on a genetic representation of a strategy. The
    required strategy is given as a table of situation->speed rules and the 
    genetic algorithm needs to approximate it.
    """
    ideal_ins = [
        [0., 0., 0.],
        [0., 0., 1.],
        [0., 1., 0.],
        [0., 1., 1.],
        [1., 0., 0.],
        [1., 0., 1.],
        [1., 1., 0.],
        [1., 1., 1.],
    ]
    
    TURN_SPEED = 0.05
    FWD_SPEED = 0.2
    
    ideal_outs = [
        [-TURN_SPEED + FWD_SPEED, TURN_SPEED + FWD_SPEED],
        [TURN_SPEED + FWD_SPEED, -TURN_SPEED + FWD_SPEED], # right
        [FWD_SPEED, FWD_SPEED],
        [FWD_SPEED, FWD_SPEED],
        [-TURN_SPEED + FWD_SPEED, TURN_SPEED + FWD_SPEED], # left
        [FWD_SPEED, FWD_SPEED],
        [FWD_SPEED, FWD_SPEED],
        [FWD_SPEED, FWD_SPEED],
    ]
    
    optimizer = controllers.GAImitationSpeedOptimizer(freq, ideal_ins, ideal_outs)
    best_chromosome = optimizer.optimize()
    
    ga_controller = controllers.GAController(freq, best_chromosome)
            
    simulation.robot.attachController(ga_controller)
         
def chooseGAOptimizationController(simulation, freq) :
    """ Attach a GA controller to the simulation.
    
    The algorithm is based on a genetic representation of a strategy. The
    genetic algorithm tries to find an optimal (suboptimal) strategy using
    a criterium that allows it to compare different control rule sets between
    themselves. The criterium used in this case is how far along the trajectory
    a robot goes in a simulation using the given rules.
    """
    optimizer = controllers.GAOptimizer(*simulation_args)
    best_chromosome = optimizer.optimize()
    
    ga_controller = controllers.GAController(freq, best_chromosome)
    
    simulation.robot.attachController(ga_controller)
    
def chooseFuzzyLogicController(simulation, freq) :
    """ Attach a fuzzy logic controller to the simulation.
    
    Uses a simple set of manually generated (by a person) rules and fuzzy logic
    to control the robot.
    """
    simulation.robot.attachController(controllers.FuzzyLogicController(freq))

if __name__ == '__main__' :
    print('Starting 2D world simulation for mobile robots')

    FREQ = 5.

    # Initialize the drawing and simulation engine
    pygame.init()
    
    robot_params = [(3.0, 2.0), 
                    -math.pi/2        
    ]
    
    path_params = [
                    ((3.0, 2.0),(4.0, 2.0), 
                     (5.0, 5.0), (5.0, 6.0),
                     (1.0, 6.0), (3.0, 2.0)),
                   0.1
    ]
    
    simulation_args = [ FREQ, 
                        robot_params,
                        path_params,
                        False] # change to True to slow down visualization (and 
                        # simulation for easier viewing)
    
    testSim = sim.OneSimulation(*simulation_args)
    testSim.addVisualization(visualization.Visualization())
    testSim.attachTimer(pygame.time.Clock())
    
    #--------------------------------------------------------------------
    # Manual ON-OFF rules
    #--------------------------------------------------------------------
    chooseOnOffController(testSim, FREQ)
    #--------------------------------------------------------------------
    
    #--------------------------------------------------------------------
    # ANN rule imitation
    #--------------------------------------------------------------------
#    chooseANNController(testSim, FREQ)
    #--------------------------------------------------------------------
    
    #--------------------------------------------------------------------
    # GA chromosome rule imitation
    #--------------------------------------------------------------------
#    chooseGASimpleRuleController(testSim, FREQ)
    #--------------------------------------------------------------------
    
    #--------------------------------------------------------------------
    # GA speed rule imitation
    #--------------------------------------------------------------------
#    chooseGASpeedRuleController(testSim, FREQ)
    #--------------------------------------------------------------------
    
    #--------------------------------------------------------------------
    # GA optimization
    #--------------------------------------------------------------------
#    chooseGAOptimizationController(testSim, FREQ)
    #--------------------------------------------------------------------
    
    #--------------------------------------------------------------------
    # Fuzzy logic controller
    #--------------------------------------------------------------------
#    chooseFuzzyLogicController(testSim, FREQ)
    #--------------------------------------------------------------------
    
    testSim.execute()
    
    dist_sum, n_intersects, q_points, dists = controllers.estimate_quality(testSim.path, 
                                 geom.linestring.LineString(testSim.robot_path_points)
                                 )    
    print('rays intersected: {}\ndist sum: {}'.format(n_intersects, dist_sum))
    
    # Uncomment to show points of intersection of rays and robot's
    # trajectory after it completes a round
#    testSim.visualizeQuality() 
    
    # NOTE: trying to close the window with simulation GUI will stop one simulation

    print('Simulation finished')
        
    wait() # wait for a keypress after finishing
        
    # Be IDLE friendly
    pygame.quit()
        