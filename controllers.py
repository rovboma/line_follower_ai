# -*- coding: utf-8 -*-
"""
Created on Mon Mar 20 15:15:18 2017

@author: Maxim Rovbo

Thanks to:
https://deap.readthedocs.io/en/1.0.x/examples/index.html
"""

import simulation as sim

import shapely.geometry as geom
from shapely import affinity

import pygame

import random

from deap import base
from deap import creator
from deap import tools

from ffnet import ffnet, mlgraph

import skfuzzy as fuzz

import math
import numpy

def estimate_quality(ideal_path, robot_path) :
    """ Estimates quality of robot trajectory following. The ideal_path should 
    be a closed contour. Its geometric center is used as a 'center'. From that
    center, rays are constructed in different directions after regular angle intervals.
    Two intersections with this ray are used: one with the trajectory itself (
    and there should be only one: complex trajectories are not supported) and 
    the first intersection with the robot's trajectory (for simplicity). 
    The result is the sum of distances between the corresponding pairs of intersections.
    The less the sum - the more similar are the trajectories. 
    If the ray intersects only the ideal path, then the distance is considered 
    to be extremely high (inf) and the robot is considered to never have finished
    the route, which counts as the worst result.
    
    Args:
        ideal_path: The path to be followed by the robot.
        robot_path: The path that the robot had actually travelled through.
        
    Returns: A float number that is the comparison of the two trajectories and 
        extra information.
    """
    ray = None
    number_of_rays = 180
    
    phi = 0.
    angle_step = 2 * math.pi / number_of_rays
    
    dist_diffs = []
    dist_sum = 0.
    
    ray_len = 10.0 # to be sure that it will intersect stuff
    
    q_points = []

    num_intersected_rays = 0   
    for i in range(0, number_of_rays) :
        ray = affinity.rotate(
            geom.LineString([ideal_path.centroid, (ideal_path.centroid.x + ray_len, 0.)]),
            phi,
            ideal_path.centroid,
            True
            )
        
        ideal_intersection = ray.intersection(ideal_path)
        
        robot_intersection = ray.intersection(robot_path)
        q_points.append(robot_intersection)
        
        if robot_intersection.is_empty :
            dist_diff = numpy.nan
        elif type(robot_intersection) == geom.multipoint.MultiPoint :
            dists = []
            for p in robot_intersection :
                dists.append(p.distance(ideal_intersection))
            dist_diff = min(dists)
            num_intersected_rays += 1
        else :
            dist_diff = robot_intersection.distance(ideal_intersection)
            num_intersected_rays += 1
        
        dist_diffs.append(dist_diff) # just for fun
        
        phi += angle_step
        
    dist_sum = numpy.nansum(numpy.array(dist_diffs))
        
    # return quality
    return dist_sum, num_intersected_rays, q_points, dist_diffs


class ControlLoop(object) :
    """ Encapsulates control loops for robots.
    """
    def __init__(self, freq_):
        """
        
        Args:
            freq: in Hz. Frequency of the control loop. Needed for Simulation objects
                to know when to call it.
        """
        self.freq = freq_
        self.robot = None
        self.last_cycle = 0 # last simulation cycle when the controller was processed
        
    def transform(self, sensor_data) :
        print('Warning: template control called. Inherit and reimplement instead')
        v_left = 0.2
        v_right = 0.4
        
        return v_left, v_right
        
    def process(self) :
        """ Call this method to get the next control action.
        """
        # get input
        sensor_data = self.robot.getSensorData()
        
        # produce output
        control_signal = self.transform(sensor_data)
        
        return control_signal
        
    def setRobot(self, robot_) :
        self.robot = robot_

class ANNController(ControlLoop) :
    """ Learns to be a controller with rules that are given to it.
    """
    def __init__(self, freq, ideal_ins, ideal_outs, USE_GA=False) :
        super(ANNController, self).__init__(freq)
        
        self.USE_GA = USE_GA
        
        self.train_input = ideal_ins
        self.train_output = ideal_outs
        
        conec = mlgraph((3,10,10,2))
        self.net = ffnet(conec)
        
    def train(self) :
        # optional starting weight ga optimization
        if (self.USE_GA) :
            print("FINDING STARTING WEIGHTS WITH GENETIC ALGORITHM...")
            self.net.train_genetic(self.train_input, self.train_output, 
                                   individuals=20, generations=500)
        
        #then train with scipy tnc optimizer
        print("TRAINING NETWORK...")
        self.net.train_tnc(self.train_input, self.train_output, maxfun = 1000, messages=1)
        
        # Test network
        print("TESTING NETWORK...")
        output, regression = self.net.test(self.train_input, self.train_output, iprint = 2)
    
    def transform(self, sensor_data) :
        v_left, v_right = self.net(sensor_data)
        
        print(v_left, v_right)
        
        return v_left, v_right
        

class GAImitationOptimizer(object) :
    """ Optimizes genetic controllers using GA algorithms trying to copy input
    controller chromosome.
    """
    def __init__(self, freq, ideal_ins, ideal_outs) :
        self.train_input = ideal_ins
        self.train_output = ideal_outs
        
        self.CMD_LEN = 4
        
        self.TURN_SPEED = 0.05
        self.FWD_SPEED = 0.2
        
        creator.create("FitnessMin", base.Fitness, weights=(-1.0,))
        creator.create("Individual", list, fitness=creator.FitnessMin)
        
        self.toolbox = base.Toolbox()
        # Attribute generator
        self.toolbox.register("attr_bool", random.randint, 0, 1)
        # Structure initializers
        self.toolbox.register("individual", tools.initRepeat, creator.Individual, 
            self.toolbox.attr_bool, 32)
        self.toolbox.register("population", tools.initRepeat, list, self.toolbox.individual)
    
        # Operator registering
        self.toolbox.register("evaluate", self.evalOne)
        self.toolbox.register("mate", tools.cxTwoPoint)
        self.toolbox.register("mutate", tools.mutFlipBit, indpb=0.05)
        self.toolbox.register("select", tools.selBest)

    def evalOne(self, individual):
        
        sum_err = numpy.sum(abs(numpy.array(individual) - numpy.array(self.train_output)))

        return sum_err,

    def optimize(self) :
        """ Performs genetic algorithm search of the base controller parameters
        to find the suboptimal.
        """
        random.seed(64)
        
        pop = self.toolbox.population(n=100) # 20
        self.CXPB, self.MUTPB, self.NGEN = 0.5, 0.1, 200 # 40
        
        print("Start of evolution")
        
        # Evaluate the entire population
        fitnesses = list(map(self.toolbox.evaluate, pop))
        for ind, fit in zip(pop, fitnesses):
            ind.fitness.values = fit
        
        print("  Evaluated %i individuals" % len(pop))
        
        # Begin the evolution
        for g in range(self.NGEN):
            print("-- Generation %i --" % g)
            
            # Select the next generation individuals
            offspring = self.toolbox.select(pop, len(pop))
            # Clone the selected individuals
            offspring = list(map(self.toolbox.clone, offspring))
        
            # Apply crossover and mutation on the offspring
            for child1, child2 in zip(offspring[::2], offspring[1::2]):
                if random.random() < self.CXPB:
                    self.toolbox.mate(child1, child2)
                    del child1.fitness.values
                    del child2.fitness.values
    
            for mutant in offspring:
                if random.random() < self.MUTPB:
                    self.toolbox.mutate(mutant)
                    del mutant.fitness.values
        
            # Evaluate the individuals with an invalid fitness
            invalid_ind = [ind for ind in offspring if not ind.fitness.valid]
            fitnesses = map(self.toolbox.evaluate, invalid_ind)
            for ind, fit in zip(invalid_ind, fitnesses):
                ind.fitness.values = fit
            
            print("  Evaluated %i individuals" % len(invalid_ind))
            
            # The population is entirely replaced by the offspring
            pop[:] = offspring
            
            # Gather all the fitnesses in one list and print the stats
            fits = [ind.fitness.values[0] for ind in pop]
            
            length = len(pop)
            mean = sum(fits) / length
            sum2 = sum(x*x for x in fits)
            std = abs(sum2 / length - mean**2)**0.5
            
            print("  Min %s" % min(fits))
            print("  Max %s" % max(fits))
            print("  Avg %s" % mean)
            print("  Std %s" % std)
        
        print("-- End of (successful) evolution --")
        
        self.best_ind = tools.selBest(pop, 1)[0]
        print("Best individual is %s, %s" % (self.best_ind, self.best_ind.fitness.values))
        
        return self.best_ind
       
    def transform(self, chromosome, sensor_data) :
        # split chromosome into readable chunks of commands
        strategy = [chromosome[i:i + self.CMD_LEN] 
            for i in range(0, len(chromosome), self.CMD_LEN)]
        
        state_i = self.translate_state(sensor_data)
                
        cmd = self.translate_cmd(strategy[state_i])
        
        return cmd
        
    def translate_state(self, sensor_data) :
        state_i = 0
        for bit in sensor_data:
            state_i = (state_i << 1) | int(bit)
            
        return state_i
    
    def translate_cmd(self, cmd) :
        # corrupted command genes - react with stop command
        if ( ((cmd[0] == 1) and (cmd[1] == 1)) 
            or ((cmd[2] == 1) and (cmd[3] == 1)) ):
            v_left = 1000.
            v_right = 1000.
        else :
            # correct genes
            if cmd[0] == 0  and cmd[1] == 0 and cmd[2] == 0 and cmd[3] == 0:
                v_left = 0.
                v_right = 0.
            elif cmd[0] == 0  and cmd[1] == 0 and cmd[2] == 0 and cmd[3] == 1:
                v_left = -self.TURN_SPEED + self.FWD_SPEED
                v_right = self.TURN_SPEED + self.FWD_SPEED
            elif cmd[0] == 0  and cmd[1] == 0 and cmd[2] == 1 and cmd[3] == 0:
                v_left = 0.
                v_right = -self.FWD_SPEED
            elif cmd[0] == 0  and cmd[1] == 1 and cmd[2] == 0 and cmd[3] == 0:
                v_left = self.TURN_SPEED + self.FWD_SPEED
                v_right = -self.TURN_SPEED + self.FWD_SPEED
            elif cmd[0] == 0  and cmd[1] == 1 and cmd[2] == 0 and cmd[3] == 1:
                v_left = self.FWD_SPEED
                v_right = self.FWD_SPEED
            elif cmd[0] == 0  and cmd[1] == 1 and cmd[2] == 1 and cmd[3] == 0:
                v_left = self.FWD_SPEED
                v_right = -self.FWD_SPEED
            elif cmd[0] == 1  and cmd[1] == 0 and cmd[2] == 0 and cmd[3] == 0:
                v_left = -self.FWD_SPEED
                v_right = 0.
            elif cmd[0] == 1  and cmd[1] == 0 and cmd[2] == 0 and cmd[3] == 1:
                v_left = -self.FWD_SPEED
                v_right = self.FWD_SPEED
            elif cmd[0] == 1  and cmd[1] == 0 and cmd[2] == 1 and cmd[3] == 0:
                v_left = -self.FWD_SPEED
                v_right = -self.FWD_SPEED
                
                
        return v_left, v_right

class GAImitationSpeedOptimizer(object) :
    """ Optimizes genetic controllers using GA algorithms trying to copy input
    controller.
    """
    def __init__(self, freq, ideal_ins, ideal_outs, USE_GA=False) :
        self.train_input = ideal_ins
        self.train_output = ideal_outs
        
        self.CMD_LEN = 4
        
        self.TURN_SPEED = 0.05
        self.FWD_SPEED = 0.2
        
        creator.create("FitnessMin", base.Fitness, weights=(-1.0,))
        creator.create("Individual", list, fitness=creator.FitnessMin)
        
        self.toolbox = base.Toolbox()
        # Attribute generator
        self.toolbox.register("attr_bool", random.randint, 0, 1)
        # Structure initializers
        self.toolbox.register("individual", tools.initRepeat, creator.Individual, 
            self.toolbox.attr_bool, 32)
        self.toolbox.register("population", tools.initRepeat, list, self.toolbox.individual)
    
        # Operator registering
        self.toolbox.register("evaluate", self.evalOne)
        self.toolbox.register("mate", tools.cxTwoPoint)
        self.toolbox.register("mutate", tools.mutFlipBit, indpb=0.05)
        self.toolbox.register("select", tools.selBest)

    def evalOne(self, individual):
        output = [self.transform(individual, x) for x in self.train_input]

        errors = []        
        for i in range(len(output)) :
            err = numpy.linalg.norm(numpy.array(self.train_output[i]) - numpy.array(output[i]))
            errors.append(err)
        
        sum_err = numpy.sum(errors)

        return sum_err,

    def optimize(self) :
        """ Performs genetic algorithm search of the base controller parameters
        to find the suboptimal.
        """
        random.seed(64)
        
        pop = self.toolbox.population(n=100)
        self.CXPB, self.MUTPB, self.NGEN = 0.5, 0.1, 400
        
        print("Start of evolution")
        
        # Evaluate the entire population
        fitnesses = list(map(self.toolbox.evaluate, pop))
        for ind, fit in zip(pop, fitnesses):
            ind.fitness.values = fit
        
        print("  Evaluated %i individuals" % len(pop))
        
        # Begin the evolution
        for g in range(self.NGEN):
            print("-- Generation %i --" % g)
            
            # Select the next generation individuals
            offspring = self.toolbox.select(pop, len(pop))
            # Clone the selected individuals
            offspring = list(map(self.toolbox.clone, offspring))
        
            # Apply crossover and mutation on the offspring
            for child1, child2 in zip(offspring[::2], offspring[1::2]):
                if random.random() < self.CXPB:
                    self.toolbox.mate(child1, child2)
                    del child1.fitness.values
                    del child2.fitness.values
    
            for mutant in offspring:
                if random.random() < self.MUTPB:
                    self.toolbox.mutate(mutant)
                    del mutant.fitness.values
        
            # Evaluate the individuals with an invalid fitness
            invalid_ind = [ind for ind in offspring if not ind.fitness.valid]
            fitnesses = map(self.toolbox.evaluate, invalid_ind)
            for ind, fit in zip(invalid_ind, fitnesses):
                ind.fitness.values = fit
            
            print("  Evaluated %i individuals" % len(invalid_ind))
            
            # The population is entirely replaced by the offspring
            pop[:] = offspring
            
            # Gather all the fitnesses in one list and print the stats
            fits = [ind.fitness.values[0] for ind in pop]
            
            length = len(pop)
            mean = sum(fits) / length
            sum2 = sum(x*x for x in fits)
            std = abs(sum2 / length - mean**2)**0.5
            
            print("  Min %s" % min(fits))
            print("  Max %s" % max(fits))
            print("  Avg %s" % mean)
            print("  Std %s" % std)
        
        print("-- End of (successful) evolution --")
        
        self.best_ind = tools.selBest(pop, 1)[0]
        print("Best individual is %s, %s" % (self.best_ind, self.best_ind.fitness.values))
        
        return self.best_ind
       
    def transform(self, chromosome, sensor_data) :
        # split chromosome into readable chunks of commands
        strategy = [chromosome[i:i + self.CMD_LEN] 
            for i in range(0, len(chromosome), self.CMD_LEN)]
        
        state_i = self.translate_state(sensor_data)
                
        cmd = self.translate_cmd(strategy[state_i])
        
        return cmd
        
    def translate_state(self, sensor_data) :
        state_i = 0
        for bit in sensor_data:
            state_i = (state_i << 1) | int(bit)
            
        return state_i
    
    def translate_cmd(self, cmd) :
        # corrupted command genes - react with stop command
        if ( ((cmd[0] == 1) and (cmd[1] == 1)) 
            or ((cmd[2] == 1) and (cmd[3] == 1)) ):
            v_left = 1000.
            v_right = 1000.
        else :
            # correct genes
            if cmd[0] == 0  and cmd[1] == 0 and cmd[2] == 0 and cmd[3] == 0:
                v_left = 0.
                v_right = 0.
            elif cmd[0] == 0  and cmd[1] == 0 and cmd[2] == 0 and cmd[3] == 1:
                v_left = -self.TURN_SPEED + self.FWD_SPEED
                v_right = self.TURN_SPEED + self.FWD_SPEED
            elif cmd[0] == 0  and cmd[1] == 0 and cmd[2] == 1 and cmd[3] == 0:
                v_left = 0.
                v_right = -self.FWD_SPEED
            elif cmd[0] == 0  and cmd[1] == 1 and cmd[2] == 0 and cmd[3] == 0:
                v_left = self.TURN_SPEED + self.FWD_SPEED
                v_right = -self.TURN_SPEED + self.FWD_SPEED
            elif cmd[0] == 0  and cmd[1] == 1 and cmd[2] == 0 and cmd[3] == 1:
                v_left = self.FWD_SPEED
                v_right = self.FWD_SPEED
            elif cmd[0] == 0  and cmd[1] == 1 and cmd[2] == 1 and cmd[3] == 0:
                v_left = self.FWD_SPEED
                v_right = -self.FWD_SPEED
            elif cmd[0] == 1  and cmd[1] == 0 and cmd[2] == 0 and cmd[3] == 0:
                v_left = -self.FWD_SPEED
                v_right = 0.
            elif cmd[0] == 1  and cmd[1] == 0 and cmd[2] == 0 and cmd[3] == 1:
                v_left = -self.FWD_SPEED
                v_right = self.FWD_SPEED
            elif cmd[0] == 1  and cmd[1] == 0 and cmd[2] == 1 and cmd[3] == 0:
                v_left = -self.FWD_SPEED
                v_right = -self.FWD_SPEED
                
                
        return v_left, v_right

class GAOptimizer(object) :
    """ Optimizes genetic controllers using GA algorithms.
    """
    def __init__(self, *sim_args) :
        self.sim_args = sim_args
        
        creator.create("FitnessMax", base.Fitness, weights=(1.0,))
        creator.create("Individual", list, fitness=creator.FitnessMax)
        
        self.toolbox = base.Toolbox()
        # Attribute generator
        self.toolbox.register("attr_bool", random.randint, 0, 1)
        # Structure initializers
        self.toolbox.register("individual", tools.initRepeat, creator.Individual, 
            self.toolbox.attr_bool, 32)
        self.toolbox.register("population", tools.initRepeat, list, self.toolbox.individual)
    
        # Operator registering
        self.toolbox.register("evaluate", self.evalOneSimulation)
        self.toolbox.register("mate", tools.cxTwoPoint)
        self.toolbox.register("mutate", tools.mutFlipBit, indpb=0.05)
        self.toolbox.register("select", tools.selBest)

    def evalOneSimulation(self, individual):
        print('Evaluating one individual via simulation')
        
        ga_controller = GAController(self.sim_args[0], individual)
        
        testSim = sim.OneSimulation(*self.sim_args)
        testSim.attachTimer(pygame.time.Clock())
        
        testSim.robot.attachController(ga_controller)
        
        testSim.execute()
        
        dist_sum, n_intersects, q_points, dists = estimate_quality(testSim.path, 
                                     geom.linestring.LineString(testSim.robot_path_points)
                                     )
        
        print('ray intersection: {}'.format(n_intersects))

        return n_intersects,

    def optimize(self) :
        """ Performs genetic algorithm search of the base controller parameters
        to find the suboptimal.
        """
        random.seed(64)
        
        pop = self.toolbox.population(n=20)
        self.CXPB, self.MUTPB, self.NGEN = 0.5, 0.2, 200
        self.CMD_LEN = 4
        
        print("Start of evolution")
        
        # Evaluate the entire population
        fitnesses = list(map(self.toolbox.evaluate, pop))
        for ind, fit in zip(pop, fitnesses):
            ind.fitness.values = fit
        
        print("  Evaluated %i individuals" % len(pop))
        
        # Begin the evolution
        for g in range(self.NGEN):
            print("-- Generation %i --" % g)
            
            # Select the next generation individuals
            offspring = self.toolbox.select(pop, len(pop))
            # Clone the selected individuals
            offspring = list(map(self.toolbox.clone, offspring))
        
            # Apply crossover and mutation on the offspring
            for child1, child2 in zip(offspring[::2], offspring[1::2]):
                if random.random() < self.CXPB:
                    self.toolbox.mate(child1, child2)
                    del child1.fitness.values
                    del child2.fitness.values
    
            for mutant in offspring:
                if random.random() < self.MUTPB:
                    self.toolbox.mutate(mutant)
                    del mutant.fitness.values
        
            # Evaluate the individuals with an invalid fitness
            invalid_ind = [ind for ind in offspring if not ind.fitness.valid]
            fitnesses = map(self.toolbox.evaluate, invalid_ind)
            for ind, fit in zip(invalid_ind, fitnesses):
                ind.fitness.values = fit
            
            print("  Evaluated %i individuals" % len(invalid_ind))
            
            # The population is entirely replaced by the offspring
            pop[:] = offspring
            
            # Gather all the fitnesses in one list and print the stats
            fits = [ind.fitness.values[0] for ind in pop]
            
            length = len(pop)
            mean = sum(fits) / length
            sum2 = sum(x*x for x in fits)
            std = abs(sum2 / length - mean**2)**0.5
            
            print("  Min %s" % min(fits))
            print("  Max %s" % max(fits))
            print("  Avg %s" % mean)
            print("  Std %s" % std)
        
        print("-- End of (successful) evolution --")
        
        self.best_ind = tools.selBest(pop, 1)[0]
        print("Best individual is %s, %s" % (self.best_ind, self.best_ind.fitness.values))
        
        return self.best_ind
    

class GAController(ControlLoop) :
    """ Genetic algorithm. 
    
    GA overview: http://web.cecs.pdx.edu/~mm/cs410-510-winter-2005/ga-tutorial.pdf
    and Robert Axelrod, "The Evolution of Strategies in the Iterated Prisoner's Dilemma," 1987:
    http://www-personal.umich.edu/~axe/research/Evolving.pdf
    
    The latter is also decribed in the former. The latter's chromosome encoding 
    is used in this GA controller.
    There are 3 binary sensors and 2 actuators. Lets discretize actuators (motors)
    to have only the following commands: forward, backward, stop. There are two
    separately controlled motors. The robot can also remember a short history of situations
    (for example, array of sensor data at the previous step). 
    There are different ways to encode a robot's strategy into the chromosome.
    Let's decribe one of them with the following parameters:
    each state is described using 3 binary sensors;
    robot knows current step and remembers the previous 1 step of history;
    there are 3 possible commands given separately to two motors;
    
    Strategy maps situations to actions. We will encode both as binary strings
    that will be a part of the robot's chromosome.
    
    Let's encode each action like this:
    00 00 (4 bits) - both motors are stopped
    01 00 - left motor goes forward, right is stopped
    00 01 - left motor is stopped, right goes forward
    10 00 - left motor goes backward, right is stopped
    etc.
    
    Note that out of 2^4 = 16 actions, 7 will not be valid (for example, 11 01 or 1111).
    
    Let's encode each situation like this:
    001 011 - the first 3 bits are values of sensors at the previous step and the
    rightmost 3 bits are values at the current step.
    
    Each situation is mapped to a single action. Thus, let's consider the following
    fixed sequence of possible situations (which consist of both historical and current
    sensor values):
    1. 000 000
    2. 000 001
    3. 000 010
    4. 000 011
    etc.
    
    Let's encode our strategy using this order. So, the leftmost 4 bits will
    encode an action for the 1 situation, next 4 bits - for the seconds, etc.:
    0100 0000 0001 ... - this will be a part of the robot's chromosome.
    It will encode the following strategy:
    000 000 -> 0100
    000 001 -> 0000
    000 010 -> 0001
    
    But the robot uses historical data to choose an action and it will not have
    any at the start (only its current state), so we should include some fake
    historical values (either in the chromosome, or as a fixed starting value in
    the algorithm - let's not add them to the chromosome since in this case they
    shouldn't have much effect). Thus, the chromosome will consist only of the
    strategy.
    
    Could use a base controller to control the robot and 
    optimizes its parameters. Fuzzy logic or PID controllers are recommended
    as a base. 
    """
    def __init__(self, freq_, chromosome) :
        super(GAController, self).__init__(freq_)
        
        self.CMD_LEN = 4
        
        self.TURN_SPEED = 0.05
        self.FWD_SPEED = 0.2
        
        self.chromosome = chromosome
            
    def transform(self, sensor_data) :
        # split chromosome into readable chunks of commands
        strategy = [self.chromosome[i:i + self.CMD_LEN] 
            for i in range(0, len(self.chromosome), self.CMD_LEN)]
        
        state_i = self.translate_state(sensor_data)
                
        cmd = self.translate_cmd(strategy[state_i])
        
        return cmd
        
    def translate_state(self, sensor_data) :
        state_i = 0
        for bit in sensor_data:
            state_i = (state_i << 1) | bit
            
        return state_i
    
    def translate_cmd(self, cmd) :
        # corrupted command genes - react with stop command
        if ( ((cmd[0] == 1) and (cmd[1] == 1)) 
            or ((cmd[2] == 1) and (cmd[3] == 1)) ):
            v_left = 1000.
            v_right = 1000.
        else :
            # correct genes
            if cmd[0] == 0  and cmd[1] == 0 and cmd[2] == 0 and cmd[3] == 0:
                v_left = 0.
                v_right = 0.
            elif cmd[0] == 0  and cmd[1] == 0 and cmd[2] == 0 and cmd[3] == 1:
                v_left = -self.TURN_SPEED + self.FWD_SPEED
                v_right = self.TURN_SPEED + self.FWD_SPEED
            elif cmd[0] == 0  and cmd[1] == 0 and cmd[2] == 1 and cmd[3] == 0:
                v_left = 0.
                v_right = -self.FWD_SPEED
            elif cmd[0] == 0  and cmd[1] == 1 and cmd[2] == 0 and cmd[3] == 0:
                v_left = self.TURN_SPEED + self.FWD_SPEED
                v_right = -self.TURN_SPEED + self.FWD_SPEED
            elif cmd[0] == 0  and cmd[1] == 1 and cmd[2] == 0 and cmd[3] == 1:
                v_left = self.FWD_SPEED
                v_right = self.FWD_SPEED
            elif cmd[0] == 0  and cmd[1] == 1 and cmd[2] == 1 and cmd[3] == 0:
                v_left = self.FWD_SPEED
                v_right = -self.FWD_SPEED
            elif cmd[0] == 1  and cmd[1] == 0 and cmd[2] == 0 and cmd[3] == 0:
                v_left = -self.FWD_SPEED
                v_right = 0.
            elif cmd[0] == 1  and cmd[1] == 0 and cmd[2] == 0 and cmd[3] == 1:
                v_left = -self.FWD_SPEED
                v_right = self.FWD_SPEED
            elif cmd[0] == 1  and cmd[1] == 0 and cmd[2] == 1 and cmd[3] == 0:
                v_left = -self.FWD_SPEED
                v_right = -self.FWD_SPEED
                
                
        return v_left, v_right
    
class FuzzyLogicController(ControlLoop) :
    def __init__(self, freq_) :
        super(FuzzyLogicController, self).__init__(freq_)
        
        self.TURN_SPEED = 0.05
        self.FWD_SPEED = 0.2
        
    def transform(self, sensor_data) :
                
        # Generate universe variables
        x_left = numpy.arange(0., 1.1, 0.1)
        x_right = numpy.arange(0., 1.1, 0.1)
        x_center = numpy.arange(0., 1.1, 0.1)
        
        dturn = numpy.arange(-self.TURN_SPEED, self.TURN_SPEED, 0.001)
        
        # Generate fuzzy membership functions
        left_lo = fuzz.trimf(x_left, [0., 0., 1.])
        left_hi = fuzz.trimf(x_left, [0., 1., 1.])
        right_lo = fuzz.trimf(x_right, [0., 0., 1.])
        right_hi = fuzz.trimf(x_right, [0., 1., 1.])
        center_lo = fuzz.trimf(x_center, [0., 0., 1.])
        center_hi = fuzz.trimf(x_center, [0., 1., 1.])
        
        dturn_lo = fuzz.trimf(dturn, [-self.TURN_SPEED, -self.TURN_SPEED, 0.])
        dturn_me = fuzz.trimf(dturn, [-self.TURN_SPEED, 0., self.TURN_SPEED])
        dturn_hi = fuzz.trimf(dturn, [0., self.TURN_SPEED, self.TURN_SPEED])
        
        # We need the activation of our fuzzy membership functions at these values.
        left_level_lo = fuzz.interp_membership(x_left, left_lo, sensor_data[0])
        left_level_hi = fuzz.interp_membership(x_left, left_hi, sensor_data[0])
        
        center_level_lo = fuzz.interp_membership(x_right, center_lo, sensor_data[1])
        center_level_hi = fuzz.interp_membership(x_right, center_hi, sensor_data[1])
        
        right_level_lo = fuzz.interp_membership(x_center, right_lo, sensor_data[2])
        right_level_hi = fuzz.interp_membership(x_center, right_hi, sensor_data[2])
        
        # Rules:
        # if left -> go left
        # if right -> go right
        # if center -> go forward
        # if not left and not right and not center -> go left
        
        # Now we apply this by clipping the top off the corresponding output
        # membership function with `numpy.fmin`
        active_rule = numpy.fmax(left_level_hi, 
                                 numpy.fmin(center_level_lo, 
                                            numpy.fmin(right_level_lo, left_level_lo)))
        dturn_activation_lo = numpy.fmin(active_rule, dturn_lo)  # removed entirely to 0
        
        dturn_activation_hi = numpy.fmin(right_level_hi, dturn_hi)
        dturn_activation_md = numpy.fmin(center_level_hi, dturn_me)
        
        dturn_res = numpy.zeros_like(dturn)
        
        # Aggregate all three output membership functions together
        aggregated = numpy.fmax(dturn_activation_lo,
                             numpy.fmax(dturn_activation_md, dturn_activation_hi))
        
        # Calculate defuzzified result
        dturn_res = fuzz.defuzz(dturn, aggregated, 'centroid')
        
        v_left = self.FWD_SPEED + dturn_res
        v_right = self.FWD_SPEED - dturn_res        
        
        return v_left, v_right

class OnOffController(ControlLoop) :
    """ Simple controller that tries to move robot left if it is too far to
    the right and vice versa. Can also be considered situated controller or 
    a production rule controller, albeit VERY simple.
    """
    def __init__(self, freq_) :
        super(OnOffController, self).__init__(freq_)
        
        self.TURN_SPEED = 0.05
        self.FWD_SPEED = 0.2
        
    def transform(self, sensor_data) :
        if ((sensor_data[0] == 1) 
            and (sensor_data[1] == 1)
            and (sensor_data[2] == 1)) :
                return self.goFwd()
        elif ((sensor_data[0] == 1) 
            and (sensor_data[1] == 1)
            and (sensor_data[2] == 0)) :
                return self.goFwd()
        elif ((sensor_data[0] == 1) 
            and (sensor_data[1] == 0)
            and (sensor_data[2] == 1)) :
                return self.goFwd()
        elif ((sensor_data[0] == 1) 
            and (sensor_data[1] == 0)
            and (sensor_data[2] == 0)) :
                return self.turnLeft()
        elif ((sensor_data[0] == 0) 
            and (sensor_data[1] == 1)
            and (sensor_data[2] == 1)) :
                return self.goFwd()
        elif ((sensor_data[0] == 0) 
            and (sensor_data[1] == 1)
            and (sensor_data[2] == 0)) :
                return self.goFwd()
        elif ((sensor_data[0] == 0) 
            and (sensor_data[1] == 0)
            and (sensor_data[2] == 1)) :
                return self.turnRight()
        else :
            # 0,0,0 case
            return self.turnLeft()         
        
    def goFwd(self) :
        v_left = self.FWD_SPEED
        v_right = self.FWD_SPEED
        
        return v_left, v_right
    
    def turnLeft(self) :
        v_left = -self.TURN_SPEED + self.FWD_SPEED
        v_right = self.TURN_SPEED + self.FWD_SPEED
        
        return v_left, v_right
        
    def turnRight(self) :
        v_left = self.TURN_SPEED + self.FWD_SPEED
        v_right = -self.TURN_SPEED + self.FWD_SPEED
        
        return v_left, v_right
