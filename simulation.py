# -*- coding: utf-8 -*-
"""
Created on Sun Mar 19 23:39:59 2017

@author: Maxim Rovbo
"""

import math

import controllers

import shapely.geometry as geom
import shapely.affinity as affin
import numpy as np

class Path(geom.polygon.LinearRing) :
    """ Multisegment object for robot to follow. Has width (i.e. a physical line).
    """
    def __init__(self, segments, width_) :
        super(Path, self).__init__(segments)
        self.width = width_

class DividingLine(geom.linestring.LineString) :
    """ Line that serves as the indicator of robot going full circle along the path.
    
    (Max x of the path bounding rectangle + robot starting x) / 2
    """
    def __init__(self, robot, path) :
        self.x = (robot.coords.x + path.bounds[2]) / 2
        points = (
            (self.x, path.bounds[1]),
            (self.x, path.bounds[3])
        )
        
        super(DividingLine, self).__init__(points)

    
class PathSensor(object) :
    """ A sensor that can detect a piece of the Path under it.
    """
    def __init__(self, robot, d_coords_) :
        self.coords = geom.Point(robot.coords.x + d_coords_[0], robot.coords.y + d_coords_[1])
        self.theta = robot.theta
        self.d_coords = d_coords_
        self.radius = 0.05 # radius of the sensor (how far it senses)        
        
    def getData(self): 
        """ 
        
        Returns: 1 if found line, 0 if not.
        """
        simPath = None
        # find path object in simulation
        for simObj in self.sim.sim_objects:
            if type(simObj) == Path :
                simPath = simObj
                
        if simPath is not None :
            if (self.coords.distance(simPath) 
                < self.radius + simPath.width / 2 ) :
                return 1
            else : 
                return 0
        else :
            print('Can\'t find path object')
            return 0
            
    def move(self, robot) :
        """ Move the sensor.
        """
        self.coords = geom.Point(robot.coords.x + self.d_coords[0], robot.coords.y + self.d_coords[1])
        self.coords = affin.rotate(self.coords, robot.theta, 
                                origin=(robot.coords.x,robot.coords.y), use_radians=True)
        self.theta = robot.theta
        
    def setSim(self, sim_) :
        """ Give the sensor a reference to the simulation it is in.
        """
        self.sim = sim_

class Robot(object):
    """ A specific type of wheeled robot. Should follow a black path on a white
    surface. Is a differential drive robot. Equipped with a line of sensors that
    can detect the black path under them.
    """
    def __init__(self, coords_, direction_) :
        self.coords = geom.Point(coords_)
        self.theta = direction_
        
        self.sim = None
        
        # place PathSensors on the robot
        self.sensors = []
        
        sensor_dy = 0.3 # in meters
        sensor_dx = 0.2 # in meters
        
        leftSensor = PathSensor(self, (-sensor_dx, sensor_dy))
        centerSensor = PathSensor(self, (0, sensor_dy))
        rightSensor = PathSensor(self, (sensor_dx, sensor_dy))
        
        self.sensors.append(leftSensor)
        self.sensors.append(centerSensor)
        self.sensors.append(rightSensor)
        
        # wheel speeds
        self.v_left = 0.0
        self.v_right = 0.0
        
        self.l = 0.2 # distance between wheels
        
        self.controller = None
        
    def setSim(self, sim_) :
        """ Give the robot a reference to the simulation it is in.
        """
        self.sim = sim_
        
        for sensor in self.sensors:
            sensor.setSim(sim_)
        
    def attachController(self, controller_) :
        self.controller = controller_
        self.controller.setRobot(self)
        
    def process(self) :
        v1, v2 = self.controller.process()
        self.setSpeed(v1, v2)
        
    def move(self, pos, theta_) :
        """ Move the robot and all its parts to the position.
        """
        self.coords = pos
        self.theta = theta_
        
        for sensor in self.sensors :
            sensor.move(self)
        
    def getSensorData(self) :
        data = []
        for sensor in self.sensors :
            data.append(sensor.getData())
            
        return data
        
    def setSpeed(self, v1, v2) :
        """ Set left and right wheel speeds to v1 and v2.
        """
        if (v1 > 100) or (v2 > 100) :
            self.v_left = 0.0
            self.v_right = 0.0
        else :
            self.v_left = v1
            self.v_right = v2
        
    def simulateDiffMovement(self, dt):
        """ 
        
        For equations see:
        https://chess.eecs.berkeley.edu/eecs149/documentation/differentialDrive.pdf
        """
        EPS = 1e-6
        
        x = self.coords.x
        y = self.coords.y
        
        true_theta = self.theta + math.pi/2
        
        if (abs(self.v_right - self.v_left) < EPS) :
            # almost straight movement
            v_abs = (self.v_right + self.v_left) / 2
            v = np.array((v_abs * math.cos(true_theta), v_abs * math.sin(true_theta)))
            self.move(geom.Point(x + dt * v[0], y + dt * v[1]), self.theta)
            return
        
        # not straight movement
        R = self.l / 2 * (self.v_left + self.v_right) / (self.v_right - self.v_left)
        w = (self.v_right - self.v_left) / self.l
        ICC = geom.Point(x - R * math.sin(true_theta), y + R * math.cos(true_theta))
        
        p_new =  (np.array(((math.cos(w * dt),-math.sin(w * dt),0), 
                        (math.sin(w * dt),math.cos(w * dt),0), (0,0,1)))
                        .dot(np.array((x - ICC.x,y - ICC.y,true_theta)))
                        + np.array((ICC.x, ICC.y, w * dt)))
                        
        self.move(geom.Point(p_new[0], p_new[1]), p_new[2] - math.pi / 2)
    
class Simulation(object) :
    """ Simulates the world. Provides step-by-step processing of the simulation.
    """
    def __init__(self, freq_) :
        """
        
        Args:
            freq_: in Hz. Frequency of the "real-time" simulation (it will pause if it
                runs ahead).
        """
        self.freq = freq_
        
        self.cycles_processed = 0 # number of cycles that the simulation has 
            # processed already
        self.last_cycle = 0 # when the last processing happened
        self.sim_objects = [] # objects that belong to the simulation and can
            # be simulated (processed) step by step
        
        self.world_width = 10. # in meters
        self.world_height = 10. # in meters
        
        self.robot = None
        self.path = None
        
    def restart(self) :
        """ Reinitializes the simulation parameters.
        """
        
        self.cycles_processed = 0
        
    def addSimObject(self, simObj) :
        """ Adds an object to the simulation.
        """
        self.sim_objects.append(simObj)
        if (type(simObj) == Robot) :
            self.robot = simObj
            self.robot.setSim(self)
        elif (type(simObj) == Path) :
            self.path = simObj
        
    def process(self) :
        """ Processes one simulation cycle.
        """
        # call controller every corresponding cycle
        if ((self.cycles_processed - self.robot.controller.last_cycle)*10 > (1000 / self.robot.controller.freq)) == 0 :
            self.robot.process()
            self.robot.controller.last_cycle = self.cycles_processed
            
        self.robot.simulateDiffMovement(1 / self.freq)
        
        self.cycles_processed += 1
            
class OneSimulation(Simulation) :
    """ Simulates one pass of the robot along the trajectory.
    Stops when it crosses the dividing line 3 times, so the robot can
    be stopped in the middle of the trajectory if it doesn't follow it well.
    Note that complex trajectories that pass nearby or cross the dividing line 
    themselves multiple times (more than 2) are not supported.
    
    Encapsulates the main loop of simulation. Capable of providing
    delays for realtime simulation (waits for the system clock when simulation
    is ahead) and visualization.
    """
    def __init__(self, freq_, robot_params, path_params, realtime_=False) :
        super(OneSimulation,self).__init__(freq_)
        
        self.realtime = realtime_
        
        self.addSimObject(Path(*path_params))
        self.addSimObject(Robot(*robot_params))
        
        self.times_crossed_dividing_line = 0 # number of times robot has crossed
            # the dividing line (3 times means it has gone full-circle along the path)
        
        self.dividing_line = DividingLine(self.robot, self.path)
        self.addSimObject(self.dividing_line)
        
        self.speed_up_coeff = 5.0 # for visualization
        self.vis = None
        
        self.robot_path_points = [self.robot.coords, ]
        
    def execute(self) :
        """ Performs one full simulation.
        """
        if (self.realtime) :
            print('Currently a step of simulation is set to {} Hz which '
                'corresponds to {:d} ms per cycle.'.format(self.freq, 
                int(1000 / self.freq)))
        
        MAX_STEPS = 800
        done = False # flag is set to True when the simulation should stop
        while not done:
            if (self.vis is not None) :
                # if visualization was added, draw objects of the simulation
                done |= self.vis.update()
                pass
            
            if True :
                robot_pos_before = self.robot.coords
                self.process()
                robot_pos_after = self.robot.coords
                
                self.robot_path_points.append(robot_pos_after)
                
                done |= self.robotFinished(robot_pos_before, robot_pos_after)
                
                self.last_cycle = self.cycles_processed
            
            if (self.realtime) :
                # TODO: change from fixed waiting to 'wait-if-ahead' behaviour
                self.clock.tick(self.freq * self.speed_up_coeff) # frequency of simulation
            
            if (self.cycles_processed > MAX_STEPS):
                done = True
            
    def robotFinished(self, pos0, pos1) :
        """ Increases self.times_crossed_dividing_line by 1 when robot crosses
        it and returns true if it has crossed it 3 times.
        """
        # if between the points along the X axis
        if ( ((self.dividing_line.x < pos0.x) and (self.dividing_line.x > pos1.x))
            or ((self.dividing_line.x > pos0.x) and (self.dividing_line.x < pos1.x)) ) :
            self.times_crossed_dividing_line += 1
            
        if (self.times_crossed_dividing_line > 2) :
            return True
        else :
            return False

    def attachTimer(self, timer) :
        self.clock = timer
            
    def addVisualization(self, vis_) :
        self.vis = vis_
        self.vis.setSimulation(self)
    
    def visualizeQuality(self) :
        dist_sum, n_intersects, q_points, dists = controllers.estimate_quality(self.path, 
                                     geom.linestring.LineString(self.robot_path_points)
                                     )
        print(dist_sum)
        self.vis.drawPoints(q_points)