# -*- coding: utf-8 -*-
"""
Created on Sun Mar 19 22:50:03 2017

@author: Maxim Rovbo

Thanks to https://www.pygame.org/docs/ref/draw.html
"""

import pygame

import shapely.geometry as geom
import shapely.affinity as affin

import simulation as sim

# Define the colors we will use in RGB format
BLACK = (  0,   0,   0)
WHITE = (255, 255, 255)
BLUE =  (  0,   0, 255)
GREEN = (  0, 255,   0)
RED =   (255,   0,   0)
YELLOW = (200, 200, 0)

class Visualization(object) :
    """ Visualizes simulation objects.
    """
    def __init__(self):
        # Set the height and width of the screen
        self.size = [800, 800]
        self.screen = pygame.display.set_mode(self.size)
         
        pygame.display.set_caption("Example code for the draw module")
         
        self.sim = None
        
        self.resolution = 100 # pixels / meter
        
    def setSimulation(self, sim_) :
        """
        
        Args: 
            sim_: simulation to visualize
        """
        self.sim = sim_
        
    def update(self) :
        """ Update and draw current representations of all the objects of the 
        simulation.
        
        Returns:
            True if user clicked close, False otherwise.
        """
        
        for event in pygame.event.get(): # User did something
            if event.type == pygame.QUIT: # If user clicked close
                return True # Flag that we are done so we exit this loop
                
        # Clear the screen and set the screen background
        self.screen.fill(WHITE)
        for vis_obj in self.sim.sim_objects :
            if (type(vis_obj) == sim.Robot) :
                self.drawRobot(vis_obj)
            elif (type(vis_obj) == sim.Path) :
                self.drawPath(vis_obj)
            elif (type(vis_obj) == sim.DividingLine) :
                p1 = self.toPixelCoords(geom.Point(vis_obj.coords[0]))
                p2 = self.toPixelCoords(geom.Point(vis_obj.coords[1]))
                pygame.draw.line(self.screen, GREEN, 
                                 [p1.x, p1.y], [p2.x, p2.y],
                                3)
               
        # Go ahead and update the screen with what we've drawn.
        # This MUST happen after all the other drawing commands.
        pygame.display.flip()
        
        return False
        
    def toPixelCoords(self, point):
        x = self.resolution * point.x
        y = self.size[1] - self.resolution * point.y
        
        return geom.Point(x, y)
                
    def drawRobot(self, robot) :
        """ Draws a robot.
        """
        width = 0.4 # meters. markers' width
        height = 0.6 # m. height
        p1 = geom.Point(robot.coords.x - width/2, robot.coords.y - height/2)
        p2 = geom.Point(robot.coords.x + width/2, robot.coords.y - height/2)
        p3 = geom.Point(robot.coords.x, robot.coords.y + height/2)
        p1 = affin.rotate(p1, robot.theta, origin=(robot.coords.x,robot.coords.y), use_radians=True)
        p2 = affin.rotate(p2, robot.theta, origin=(robot.coords.x,robot.coords.y), use_radians=True)
        p3 = affin.rotate(p3, robot.theta, origin=(robot.coords.x,robot.coords.y), use_radians=True)
        p1 = self.toPixelCoords(p1)
        p2 = self.toPixelCoords(p2)
        p3 = self.toPixelCoords(p3)
        # draw main body
        pygame.draw.polygon(self.screen, BLACK, 
            [[p1.x, p1.y], [p2.x, p2.y], [p3.x, p3.y]], 
            3)    

        # draw COG
        p = self.toPixelCoords(robot.coords)
        pygame.draw.circle(self.screen, BLACK,
                           [int(p.x), int(p.y)], 6 # have no idea why int() is necessary here, but not in other places
                           )
        
        # draw sensors
        sensor_w = 0.1
        sensor_h = 0.1
        for sensor in robot.sensors :
            p = sensor.coords
            p1 = geom.Point(p.x - sensor_w / 2, p.y + sensor_h / 2)
            p1 = self.toPixelCoords(p1)
            
            if sensor.getData() == 0 :
                pygame.draw.ellipse(self.screen, GREEN, [p1.x, p1.y, 
                                 sensor_w * self.resolution, sensor_h * self.resolution], 2)
            else :
                pygame.draw.ellipse(self.screen, RED, [p1.x, p1.y, 
                                 sensor_w * self.resolution, sensor_h * self.resolution])
        
    def drawPath(self, path) :
        """ Draws a path.
        """
        segments = path.coords
        for i, val in enumerate(segments):
            p1 = self.toPixelCoords(geom.Point(segments[i]))
            if (i < len(segments) - 1) :
                p2 = self.toPixelCoords(geom.Point(segments[i+1]))
            else :
                p2 = self.toPixelCoords(geom.Point(segments[0]))
                
            pygame.draw.line(self.screen, BLUE, [p1.x, p1.y], [p2.x, p2.y], 
                             int(path.width * self.resolution))
                             
    def drawPoints(self, points) :
        """ Draws points.
        """
        for p in points :
            if type(p) == geom.multipoint.MultiPoint :
                self.drawPoints(p)
            elif type(p) == geom.collection.GeometryCollection :
                for geometry in p :
                    self.drawPoints(geometry)
            else :
                p = self.toPixelCoords(p)
                pygame.draw.circle(self.screen, YELLOW,
                               [int(p.x), int(p.y)], 4 # have no idea why int() is necessary here, but not in other places
                               )
                                                      
        pygame.display.flip()